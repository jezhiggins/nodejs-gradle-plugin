//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs.tasks

import org.ysb33r.gradle.nodejs.LernaExtension
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification
import org.ysb33r.grolifant.api.OperatingSystem

class LernaTaskSpec extends UnittestBaseSpecification {

  void 'Create Lerna task'() {

    setup:
    project.apply plugin : 'org.ysb33r.nodejs.lerna'

    when: 'A Lerna task is created'
    LernaTask task = project.tasks.create('myLernaTask',LernaTask)

    then: 'Three extensions are added to the task'
    task.extensions.getByName('nodejs') instanceof NodeJSExtension
    task.extensions.getByName('npm')    instanceof NpmExtension
    task.extensions.getByName('lerna')  instanceof LernaExtension

    when: 'Configuring the task'
    project.allprojects {
      // tag::configure-lerna-task[]
      myLernaTask {
        command 'run'   // <1>
        cmdArgs 'test'  // <2>
        environment PG_CONNECTION_STRING: 'postgres://postgres:postgres@localhost:5432/test_db'   // <3>
      }
      // end::configure-lerna-task[]
    }
    def expectedClient = OperatingSystem.current().isWindows() ? 'npm.cmd' : 'npm-cli.js'

    then: 'The task can be set'
    task.command == 'run'
    task.cmdArgs == ['test']

    then: 'The default npmClient is the NpmExtension default'
    task.npmClient != null
    task.npmClient.endsWith(expectedClient)

    when: 'When an npm client is set on the npm'
    def npmPath = "${File.separator}path${File.separator}to${File.separator}npm"
    project.allprojects {
      npm {
        executable path: npmPath
      }
    }

    then: 'Then task will use that npmclient'
    task.getNpmClient().endsWith(npmPath)

    when: 'Configuring the extension'
    def ext = task.extensions.getByName('lerna')
    project.allprojects {
      lerna {
        npmClient 'something-made-up'
      }
    }

    then:
    ext.npmClient != null
    ext.npmClient.endsWith('something-made-up')
    task.npmClient != null
    task.npmClient.endsWith('something-made-up')

    when: 'When setting the npmClient on the lerna extension'
    project.allprojects {
      // tag::configure-lerna-extension[]
      myLernaTask {
        lerna {
          npmClient 'yarn'  // <1>
        }
        command 'bootstrap'
      }
      // end::configure-lerna-extension[]
    }

    then: 'The npmClient should be updated'
    task.npmClient == 'yarn'
    task.command == 'bootstrap'

    when: 'The npm client is specified on the task'
    project.allprojects {
      myLernaTask {
        npmClient 'npm-winner'
      }
    }

    then: 'That wins'
    task.npmClient == 'npm-winner'


  }
}