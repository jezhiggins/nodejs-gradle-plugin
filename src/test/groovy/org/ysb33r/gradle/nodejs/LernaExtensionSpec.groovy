//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs

import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification
import spock.lang.Specification
import org.ysb33r.grolifant.api.OperatingSystem

class LernaExtensionSpec extends UnittestBaseSpecification {

  void 'Create a Lerna extension'() {

    when: 'The Lerna plugin is applied'
    project.allprojects {
      project.apply plugin : 'org.ysb33r.nodejs.lerna'
    }

    then: 'A project extension named lerna is created'
    project.extensions.getByName('lerna') instanceof LernaExtension
    project.extensions.getByName('lerna').extensionName == 'lerna'

    and: 'The default installation group is development'
    project.extensions.getByName('lerna').installGroup == NpmDependencyGroup.DEVELOPMENT

    when: 'When no npmClient is set'
    LernaExtension lernaExt = project.extensions.getByName('lerna')
    def expectedClient = OperatingSystem.current().isWindows() ? 'npm.cmd' : 'npm-cli.js'

    then: 'The default npmClient is the NpmExtension default'
    lernaExt.npmClient.endsWith(expectedClient)

    when: 'When an npm client is set on the npm'
    def npmPath = "${File.separator}path${File.separator}to${File.separator}npm"
    project.allprojects {
      npm {
        executable path: npmPath
      }
    }
    then: 'Them lerna will use that npmclient'
    lernaExt.getNpmClient().endsWith(npmPath)

    when: 'When setting the npmClient on the lerna extension'
    project.allprojects {
      lerna {
        npmClient 'something-made-up'
      }
    }

    then: 'The npmClient should be updated'
    lernaExt.getNpmClient() == 'something-made-up'

    when: 'The npmClient is set by assignment'
    project.allprojects {
      // tag::configuring-lerna[]
      lerna {
        executable version : '2.0.0'         // <1>
        npmClient = 'yarn'                   // <2>
      }
      // end::configuring-lerna[]
    }

    then: 'The npmClient should be replaced'
    lernaExt.npmClient == 'yarn'

    when: 'The installation group is updated'
    project.allprojects {
      // tag::change-installation-group[]
      lerna {
        installGroup NpmDependencyGroup.OPTIONAL // <1>
      }
      // end::change-installation-group[]
    }

    then: 'The installation group will be updated'
    lernaExt.installGroup == NpmDependencyGroup.OPTIONAL


  }
}