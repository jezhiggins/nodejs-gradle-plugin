//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs.pkgwrapper

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.SimpleNpmPackageDescriptor
import org.ysb33r.gradle.nodejs.impl.Downloader
import org.ysb33r.gradle.nodejs.impl.npm.NpmPackageResolver
import org.ysb33r.grolifant.api.StringUtils
import org.ysb33r.grolifant.api.exec.AbstractToolExecSpec
import org.ysb33r.grolifant.api.exec.ExternalExecutable
import org.ysb33r.grolifant.api.exec.NamedResolvedExecutableFactory
import org.ysb33r.grolifant.api.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.exec.ResolveExecutableByVersion
import org.ysb33r.grolifant.api.exec.ResolvedExecutableFactory
import org.ysb33r.grolifant.api.exec.ResolverFactoryRegistry

/** A base class for creating resolvers for specific NPM packages.
 *
 * <p> This class is meant to be used when creating an ecosystem for specific broad-use
 * NPM-based tools such as Gulp.
 *
 * @since 0.2
 */
@CompileStatic
class TagBasedPackageResolver implements NamedResolvedExecutableFactory {

    /** Obtains the installation group for a package
     *
     */
    static interface InstallGroup {
        NpmDependencyGroup getGroup()
    }

    /** Obtains the entry point for package.
     *
     */
    static interface EntryPoint {
        String getEntryPoint()
    }

    /**
     *
     * @param scope Resolve scope. Can be {@code null}.
     * @param packageName The name of the package that will be resolved.
     * @param project Project this is associated with
     * @param nodeExtension Node extension that should be used for resolving Node questions.
     * @param npmExtension NPM extension that shgoul dbe used for resolving NPM questions.
     * @param installGroupLocator Ability to obtain the installation group for a package when package needs to be resolved.
     * @param entryPointLocator Ability to obtain the entry point for a package when package needs to be resolved.
     */
    TagBasedPackageResolver(
        final String scope,
        final String packageName,
        Project project,
        NodeJSExtension nodeExtension,
        NpmExtension npmExtension,
        InstallGroup installGroupLocator,
        EntryPoint entryPointLocator

    ) {
        this.scope = scope
        this.packageName = packageName
        this.groupLocator = installGroupLocator
        this.entryPointLocator = entryPointLocator
        this.npmPackageResolver = new NpmPackageResolver(project,nodeExtension,npmExtension)
    }

    /** Returns a name that can be used as a key into a {@link ResolverFactoryRegistry}.
     *
     * @return Always return 'version'.
     */
    @Override
    String getName() {
        'version'
    }


    @Override
    ResolvableExecutable build(Map<String, Object> options, Object from) {
        return new ResolvableExecutable() {
            @Override
            File getExecutable() {
                SimpleNpmPackageDescriptor descriptor = new SimpleNpmPackageDescriptor(
                    scope,
                    packageName,
                    StringUtils.stringize(from)
                )
                npmPackageResolver.resolvesWithEntryPoint(descriptor,entryPointLocator.entryPoint,groupLocator.group)
            }
        }
    }


    private final String scope
    private final String packageName
    private final InstallGroup groupLocator
    private final EntryPoint entryPointLocator
    private final NpmPackageResolver npmPackageResolver

//    /** Set the package executable to be resolved.
//
//     */
//    void executable(Map<String,Object> opts) {
////        factory.setExecutable(opts)
//    }
//
//    ResolvableExecutable getResolvableExecutable(final String entryPoint, final NpmDependencyGroup installGroup) {
////        factory.entryPoint = entryPoint
////        factory.installGroup = installGroup
////        factory.resolvableExecutable
//    }


//    private static class Version implements ResolvedExecutableFactory {
//
//        Version(final Factory factory) {
//            this.factory = factory
//        }
//
//        /** Creates {@link ResolvableExecutable} from a Gulp tag.
//         *
//         * @param options Ignored
//         * @param from Anything convertible to a string that contains a valid NPM executable tag.
//         * @return The resolved executable.
//         */
//        @Override
//        ResolvableExecutable build(Map<String, Object> options, Object from) {
//            return new ResolvableExecutable() {
//                @Override
//                File getExecutable() {
//                    SimpleNpmPackageDescriptor descriptor = new SimpleNpmPackageDescriptor(factory.scope,factory.packageName,from.toString())
//                    factory.resolver.resolvesWithEntryPoint(descriptor,factory.entryPoint,factory.installGroup)
//                }
//            }
//        }
//        private Factory factory
//    }
//
//    private static class Factory  {
//        Factory(final String scope, final String packageName, Project project, NodeJSExtension nodeExtension, NpmExtension npmExtension) {
//            this.project = project
//            this.scope = scope
//            this.packageName = packageName
//            this.project = project
//            this.resolver = new NpmPackageResolver(project,nodeExtension,npmExtension)
//            this.registry = new ResolverFactoryRegistry(project)
////            registerExecutableKeyActions('version',new Version(this))
//
//            ResolveExecutableByVersion.DownloaderFactory downloaderFactory = {
//                Map<String, Object> options, String version,Project p ->
//                    new Downloader(version,p)
//            } as ResolveExecutableByVersion.DownloaderFactory
//
//            ResolveExecutableByVersion.DownloadedExecutable resolver = { Downloader installer ->
//                SimpleNpmPackageDescriptor descriptor = new SimpleNpmPackageDescriptor(factory.scope,factory.packageName,from.toString())
//                factory.resolver.resolvesWithEntryPoint(descriptor,factory.entryPoint,factory.installGroup)
//            } as ResolveExecutableByVersion.DownloadedExecutable
//
//            this.registry.registerExecutableKeyActions(
//                new ResolveExecutableByVersion(project,downloaderFactory,resolver)
//            )
//        }
//
//        final String scope
//        final String packageName
//        final Project project
//        final NpmExtension npm
//        final NodeJSExtension node
//        final NpmPackageResolver resolver
//        final ResolverFactoryRegistry registry
//
//        NpmDependencyGroup installGroup
//        String entryPoint
//    }
//
//    private final Factory factory

}
