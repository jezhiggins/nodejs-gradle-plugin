//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.apache.tools.ant.types.LogLevel
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant.api.StringUtils
import org.ysb33r.gradle.nodejs.LernaExtension
import org.ysb33r.gradle.nodejs.NodeJSExecSpec

/** Ability to run a Lerna task.
 *
 * <p> If the project defaults for {@code nodejs{, {@code npm} and {@code lerna} is not to requirement, each
 * of those can be configure via task extensions by the same name.
 *
 * @since 0.3
 */
@CompileStatic
class LernaTask extends AbstractNodeBaseTask {

  LernaTask() {
    super()
    this.lernaExtension = (LernaExtension)(extensions.create(LernaExtension.NAME,LernaExtension,this))
  }

  /** The Lerna command to run.
   *
   * @return command to run
   */
  @Input
  String getCommand() {
    if(this.command == null) {
      null
    } else {
      StringUtils.stringize(this.command)
    }
  }

  /** Set the Lerna command to run.
   *
   * @param commandName
   */
  void setCommand(final Object command) {
    this.command = command
  }

  /** Set the Lerna command to run.
   *
   * @param commandName
   */
  void command(final Object command) {
    setCommand(command)
  }

  /** Replace the command-specific arguments with a new set.
   *
   * @param args New list of command-specific arguments
   */
  void setCmdArgs(Iterable<?> args) {
    this.commandArgs.clear()
    this.commandArgs.addAll(args)
  }

  /** Add more command-specific arguments.
   *
   * @param args Additional list of arguments
   */
  void cmdArgs(Iterable<?> args) {
    this.commandArgs.addAll(args)
  }

  /** Add more command-specific arguments.
   *
   * @param args Additional list of arguments
   */
  void cmdArgs(Object... args) {
    this.commandArgs.addAll(args)
  }

  /** Any arguments specific to the command.
   *
   * @return Arguments to the commands. Can be empty, but never null.
   */
  List<String> getCmdArgs() {
    return StringUtils.stringize(this.commandArgs)
  }

  /** The NPM client to use.
   *
   * @return npm client path
   */
  @Input
  String getNpmClient() {
    return this.npmClientPath ?
        this.npmClientPath :
        this.lernaExtension.npmClient
  }

  /** Set the npm client path
   *
   * @param path the to npm client
   */
  void setNpmClient(final String npmClientPath) {
    this.npmClientPath = npmClientPath
  }

  @TaskAction
  void exec() {
    NodeJSExecSpec execSpec = createExecSpec()

    execSpec.script this.lernaExtension.resolvableExecutable.executable.absolutePath

    switch (project.logging.level) {
      case LogLevel.QUIET:
        execSpec.scriptArgs '--loglevel', 'silent'
        break;
      case LogLevel.INFO:
        execSpec.scriptArgs '--loglevel', 'info'
        break;
      case LogLevel.DEBUG:
        execSpec.scriptArgs '--loglevel', 'verbose'
        break;
    }

    if(this.command) {
      execSpec.scriptArgs this.getCommand()
    }

    if('clean'.equals(this.getCommand())) {
      execSpec.scriptArgs '--yes'
    }

    execSpec.scriptArgs "--npm-client=${this.getNpmClient()}"

    execSpec.scriptArgs this.getCmdArgs()

    setupPath(execSpec)

    runExecSpec(execSpec)
  } // exec

  private setupPath(final NodeJSExecSpec execSpec) {
    String currentPath = System.getenv('PATH')
    String npmPath = parentOf(this.getNpmClient())
    String nodePath = parentOf(this.lernaExtension.getNodeExecutable())

    String sep = File.pathSeparator

    String newPath = "${currentPath}${sep}${npmPath}${sep}${nodePath}"

    execSpec.environment('PATH', newPath)
  } // setupPath

  private String parentOf(final String executable) {
    return new File(executable).parent
  } // parentOf

  private Object command
  private List<Object> commandArgs = []

  private String npmClientPath
  private final LernaExtension lernaExtension

}
