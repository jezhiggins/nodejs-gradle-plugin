//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.pkgwrapper.AbstractPackageWrappingExtension
import org.ysb33r.gradle.nodejs.tasks.LernaTask
import org.ysb33r.grolifant.api.OperatingSystem

/** Extension that allows for setting of Lerna configuration at a project or task level.
 *
 * If no executable is set the default will be to install {@link #LERNA_DEFAULT} for a project extension. In the case of
 * a task extension it will default to the project extension's settings.
 *
 * @since 0.1
 */
@CompileStatic
class LernaExtension extends AbstractPackageWrappingExtension {

  final static String NAME = 'lerna'

  /**
   *  The default version of lerna that will be used if nothing is configured.
   */
  final static String LERNA_DEFAULT = '2.5.1'

  /** Adds the extension to the project.
   *
   * <p> Sets the default Lerna tag to {@code LERNA_DEFAULT}.
   *
   * @param project Project to link to.
   */
  LernaExtension(Project project) {
    super(project,NAME)
    setInstallGroup(NpmDependencyGroup.DEVELOPMENT)
    executable( [version : LERNA_DEFAULT] as Map<String,Object> )
    this.defaultNpmClientPath = npmExtensionNpmClient()
  }

  /** Adds the extension to a {@link LernaTask} task.
   *
   * <p> Links the executable resolving to the global instance, but allows
   * it to be overriden on a per-task basis.
   *
   * @param task Task to be extended.
   */
  LernaExtension(LernaTask task) {
    super(task,NAME)
  }

  /** Sets the {@npmClient} to use
   *
   * @param npmClient path
   */
  void npmClient(Object npmClientPath) {
    setNpmClient(npmClientPath)
  }

  /** Sets the {@npmClient} to use
   *
   * @param npmClient name
   */
  void setNpmClient(Object npmClientPath) {
    this.npmClientPath = npmClientPath
  }

  /** The {@code npmClient} to use
   *
   * @return Returns npmClient lerna should use when running commands
   */
  String getNpmClient() {
    if (this.npmClientPath)
      return this.npmClientPath

    if (this.task) {
      LernaExtension lernaExt = (LernaExtension) extension(LernaExtension.NAME)
      return lernaExt.npmClient
    }

    String npmClient = npmExtensionNpmClient()

    if (OperatingSystem.current().isWindows()) {
      // ok, if we get handed the default 'npm-cli.js' we can't use that on Windows
      String npmCli = "node_modules${File.separator}npm${File.separator}bin${File.separator}npm-cli.js"
      if (npmClient == this.defaultNpmClientPath && npmClient.endsWith(npmCli)) {
        // it is, and nothing else has been set, so let's fiddle it
        npmClient = "${npmClient.substring(0, npmClient.length() - npmCli.length())}npm.cmd"
      }
    }

    return npmClient
  } // getNpmClient

  String getNodeExecutable() {
    NodeJSExtension nodeExt = (NodeJSExtension)extension(NodeJSExtension.NAME)
    File node = nodeExt.resolvableNodeExecutable.executable
    return node.absolutePath
  } // getNodeExecutable

  private String npmExtensionNpmClient() {
    NpmExtension npmExt = (NpmExtension)extension(NpmExtension.NAME)
    File npm = npmExt.resolvedNpmCliJs.executable
    return npm.absolutePath
  } // npmExtensionNpmClient

  private def extension(String name) {
    return this.project.extensions.getByName(name)
  } // extension

  /** Returns the name by which the extension is known.
   *
   * @return Extension name. Never null.
   */
  @Override
  protected String getExtensionName() {
    NAME
  }

  /** The entrypoint path relative to the installed executable folder
   *
   * @return {@code bin/lerna.js}
   */
  @Override
  protected String getEntryPoint() {
    'bin/lerna.js'
  }

  private Object npmClientPath
  private Object defaultNpmClientPath
}
