//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.nodejs.downloadtest

import org.ysb33r.gradle.nodejs.downloadtest.helper.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.downloadtest.helper.NpmBaseTestSpecification
import org.ysb33r.gradle.nodejs.impl.npm.NpmExecutor
import org.ysb33r.gradle.nodejs.tasks.LernaTask

import java.nio.file.Files
import java.nio.file.StandardCopyOption

class LernaSpec extends NpmBaseTestSpecification {

  def 'Create task and run Lerna task'() {
    setup:
    File lernaJson = new File(project.projectDir,'lerna.json')
    Files.copy( new File(DownloadTestSpecification.RESOURCES_DIR,'simple-lerna.json').toPath(),lernaJson.toPath(),StandardCopyOption.COPY_ATTRIBUTES)

    project.allprojects {
      apply plugin : 'org.ysb33r.nodejs.lerna'
    }

    NpmExecutor.initPkgJson(project,project.nodejs,project.npm)

    LernaTask lerna = project.tasks.create('helloWorld',LernaTask)
    lerna.command = 'ls'

    when:
    project.evaluate()
    lerna.execute()

    then:
    noExceptionThrown()

  }
}