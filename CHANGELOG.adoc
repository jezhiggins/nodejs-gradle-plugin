= Changelog

== v0.4 - Roadmap

* https://github.com/ysb33r/nodejs-gradle-plugin/issues/1[#1] - Improve caching of `NpmPackageJsonInstall` task type.
* https://github.com/ysb33r/nodejs-gradle-plugin/issues/2[#2] - Map Node/NPM/Gulp logging levels to Gradle levels.

// tag::changelog[]
== v0.3
* Lerna support

// end::changelog[]

== v0.2

* https://github.com/ysb33r/nodejs-gradle-plugin/issues/5[#5] - Move project from Schalk Cronjé's personal repository on GitHub to *Ysb33r Software Foundation* repository on GitLab.
* Upgrade to Grolifant 0.5 and refactor local codebase.

== v0.1

First release:

* Download Node.js on supported platforms
* NPM support
* Gulp support

